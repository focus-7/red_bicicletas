var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");

describe("Testing bicicletas", () => {

  beforeAll(function (done) {
    mongoose.connection.close().then(() => {
      var mongoDB = "mongodb://localhost/testdb";

      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      mongoose.set("useCreateIndex", true);

      var db = mongoose.connection;
      db.on("error", console.error.bind(console, "MongoDB connection error: "));
      db.once("open", function () {
        console.log("We are connected to test database!");
        done();
      });
    });
  });
  afterEach((done) => {
    Bicicleta.deleteMany({}, function (error, success) {
      if (error) console.log(error);
      done();
    });
  });

  describe("Bicicleta.createInstance", () => {
    it("crear una instancia de bicicleta", (done) => {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.1, -54]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toEqual(-34.1);
      expect(bici.ubicacion[1]).toEqual(-54);

      done();
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("comienza vacia", (done) => {
      Bicicleta.allBicis((err, bicis) => {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta.add", () => {
    it("agrega solo una bicicleta", (done) => {
      var aBici = new Bicicleta({ code: 1, color: "rojo", modelo: "montaña" });
      Bicicleta.add(aBici, function (err, bici) {
        if (err) console.log(bici);
        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toBe(1);
          expect(bicis[0].code).toBe(aBici.code);
          done();
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("devolver bici con code 1", (done) => {
      expect(Bicicleta.allBicis.length).toBe(1);

      var aBici1 = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });

      Bicicleta.add(aBici1, function (err, newBici) {
        if (err) console.log(err);
        var aBici2 = new Bicicleta({
          code: 2,
          color: "roja",
          modelo: "urbana",
        });

        Bicicleta.add(aBici2, function (err, newBici) {
          if (err) console.log(err);
          Bicicleta.findByCode(1, function (err, targetBici) {
            if (err) console.log(err);
            expect(targetBici.color).toBe(aBici1.color);
            expect(targetBici.modelo).toBe(aBici1.modelo);
            expect(targetBici.code).toBe(aBici1.code);

            done();
          });
        });
      });
    });
  });
});

/* beforeEach(() => {Bicicleta.allBicis = []})

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});


describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, "rojo", "urbana");
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toEqual(a);
    });
});


describe('Bicicleta.findById', () => {
    it('encontrar bicicleta con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, "verde", "urbana");
        Bicicleta.add(a);

        var biciUno = Bicicleta.findById(1);

        expect(biciUno.id).toBe(1);
        expect(biciUno.color).toBe("verde");    
        expect(biciUno.modelo).toBe("urbana");
    });
});


describe('Bicicleta.removeById', () => {
    it('eliminar bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, "dorada", "montaña");
        var b = new Bicicleta(2, "blanca", "urbana");
        Bicicleta.add(a);
        Bicicleta.add(b);

        Bicicleta.removeById(1);

        expect(Bicicleta.allBicis.length).toBe(1);
    });
}); */
